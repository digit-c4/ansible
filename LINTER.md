# Linter

For ansible, we have a linter package inside gitlab, which *MUST* be used.
To configure it, put the following content inside your .gitlab-ci.yml

```
default:
  tags:
     - lab
     - shell

stages:
  - default_validate

include:
  - project: 'digit-c4/digitC4-template-cicd'
    file: 'gitlab-ci-ansible.yml'
    ref: main
```

The merge requests should be declined if the linter is failling.
